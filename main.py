import telebot
import os
import random
import config2

token = config2.token

bot = telebot.TeleBot(token, parse_mode=None)


def get_random_file(directory):
    files = os.listdir(directory)
    file_name = files[random.randint(0, len(files) - 1)]
    return file_name


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    bot.reply_to(message, "Этот бот присылает картинки разных котиков.")


@bot.message_handler(commands=['white_cat', 'red_cat', 'wb_cat'])
def get_cat(message):
    command = message.text[1:]
    directories = {'white_cat': 'images/white_cats', 'red_cat': 'images/red_cats', 'wb_cat': 'images/wb_cats'}
    file_name = get_random_file(directories[command])
    bot.send_photo(message.chat.id, photo=open(directories[command] + '/' + file_name, 'rb'))


@bot.message_handler(content_types=['location'])
def handle_location(message):
    bot.send_message(message.chat.id, 'Теперь я знаю, где ты находишься 😝! Твои координаты: %s %s' % (message.location.longitude, message.location.latitude))

bot.infinity_polling()
